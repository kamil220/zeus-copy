import Swiper from "swiper";

const imageSlider = document.querySelectorAll('.image-slider__swiper');
    if (imageSlider.length > 0) {
        imageSlider.forEach(slider => {
            const attributes = {
                speed: parseInt(slider.getAttribute('data-speed')),
                autoplay: parseInt(slider.getAttribute('data-autoplay')),
                arrows: JSON.parse(slider.getAttribute('data-arrows')),
                direction: JSON.parse(slider.getAttribute('data-direction')),
                pauseonhover: JSON.parse(slider.getAttribute('data-pauseonhover')),
            };

            let startSliding;
            if (attributes.direction) {
                startSliding = slider.querySelectorAll('.swiper-slide').length;
            } else {
                startSliding = 0;
            }

            const config = {
                spaceBetween: 10,
                speed: attributes.speed,
                navigation: {
                    ...attributes.arrows && {
                        nextEl: '.swiper-button-next'
                    },
                    ...attributes.arrows && {
                        prevEl: '.swiper-button-prev'
                    },
                },
                breakpoints: {
                    320: {
                        slidesPerView: 2,
                        spaceBetween: 20
                    },
                    480: {
                        slidesPerView: 3,
                        spaceBetween: 30
                    },
                    640: {
                        slidesPerView: 4,
                        spaceBetween: 40
                    }
                },
                initialSlide: startSliding,
                autoplay: {
                    delay: attributes.autoplay,
                    reverseDirection: attributes.direction,
                }
            }

            const imageSliderSwiper = new Swiper(slider, config);

            if (attributes.pauseonhover) {
                slider.addEventListener('mouseover', () => imageSliderSwiper.autoplay.stop());
                slider.addEventListener('mouseout', () => imageSliderSwiper.autoplay.start());
            }
        });
    }
