import Swiper from "swiper";

const testimonialsSliders = document.querySelectorAll('.testimonials__swiper');

    if (testimonialsSliders.length > 0) {
        testimonialsSliders.forEach(slider => {
            const swiper = new Swiper(slider, {
                slidesPerView: 1,
                centeredSlides: true,
                navigation: {
                    nextEl: slider.nextElementSibling,
                    prevEl: slider.previousElementSibling,
                },
            });

            const headerSlider = slider.offsetParent.querySelector('.testimonials__header-swiper');
            const headerSwiper = new Swiper(headerSlider, {
                slidesPerView: 1,
                centeredSlides: true,
                touchRatio: 0,
            });

            const testimonialsPersons = slider.offsetParent.querySelector('.testimonials__persons');

            if (testimonialsPersons !== null) {
                const persons = testimonialsPersons.querySelectorAll('.testimonials__person--inner');

                const removeActive = () => persons.forEach(person => person.classList.remove('active'));

                persons.forEach(person => {
                    person.addEventListener('click', () => {
                        swiper.slideTo(parseInt(person.getAttribute(
                            'data-slide-index')));
                        removeActive();
                        person.classList.add('active');
                    });
                });

                swiper.on('slideChange', function () {
                    removeActive();
                    persons[swiper.activeIndex].classList.add('active');
                    headerSwiper.slideTo(swiper.activeIndex);
                });
            }
        });
    }
