import {
    slideDown,
    slideUp,
    slideStop,
} from 'slide-anim';

const accordions = document.querySelectorAll('.ww-accordions');

if (accordions.length > 0) {
    accordions.forEach(accordion => {
        const articles = accordion.querySelectorAll('.ww-accordion');

        articles.forEach(article => {
            const trigger = article.querySelector('.ww-accordion__trigger');
            const content = article.querySelector('.ww-accordion__content');

            trigger.addEventListener('click', e => {

                if (trigger.classList.contains('read-more')) {
                    e.preventDefault();

                    if (article.classList.contains('active')) {
                        // If active accordion is clicked, hide it
                        article.classList.remove('active');
                        slideStop(content);
                        slideUp(content);
                    } else {
                        // Remove active from all articles
                        articles.forEach(singleArticle => singleArticle.classList.remove('active'));

                        // Add active to only one article

                        article.classList.add('active');

                        slideStop(content);
                        slideDown(content, {
                            duration: 500,
                            display: 'flex',
                        });

                    }
                }
            });
        });

        const togglers = document.querySelectorAll('.ww-accordions__toggler');
        togglers.forEach(toggler => {
            toggler.addEventListener('click', () => {
                toggler.classList.toggle('active');

                if (accordion.classList.contains('active')) {
                    slideDown(accordion);
                } else {
                    slideUp(accordion);
                }

                accordion.classList.toggle('active');
            });
        });

    });
}