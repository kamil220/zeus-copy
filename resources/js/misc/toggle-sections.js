import {
    slideDown,
    slideUp,
    slideStop,
} from 'slide-anim';

const sections = document.querySelectorAll('.ww-toggler-section');

if (sections.length > 0) {
        const togglers = document.querySelectorAll('.ww-section__toggler');
        togglers.forEach(toggler => {
            toggler.addEventListener('click', () => {
                toggler.classList.toggle('active');

                sections.forEach( section => {
                    if (!section.classList.contains('active')) {
                        slideDown(section);
                    } else {
                        slideUp(section);
                    }

                    section.classList.toggle('active');
                });

            });
        });
}
