( function( $ ) {
    const wpcf7Elm = document.querySelector( '.wpcf7' );

    if( typeof wpcf7Elm == 'undefined' || !wpcf7Elm ) {
        return;
    }

    wpcf7Elm.addEventListener( 'wpcf7mailsent', function( event ) {
        $.ajax({
            type : "POST",
            dataType : "json",
            url : zeus_object.ajax_url,
            data : {
                action: "ww_cf7_submit_form",
                form_id : event.detail.contactFormId,
            },
            success: function(response) {

                if (response.data.status !== 'success') {
                    return;
                }
                
                $( '#' + event.detail.id )
                    .addClass( 'ww-custom-thank-you-message' )
                    .find( '.wpcf7-response-output' )
                    .html( response.data.html );
            }
        });

    }, false );
})(jQuery);
