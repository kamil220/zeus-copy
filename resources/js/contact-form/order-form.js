( function( $ ) {

    const wpcf7Elm = document.querySelector( '.wpcf7' );
    let $units = $( '.ww-units-numbers' );
    let $patient_details = $( '.ww-patient-details-container .ww-patient' );
    let $patient_personalize = $( '.ww-patient-personalize-container .ww-patient' );
    const $subheader_to_hide = $('.ww-subheader-to-hide');
    const $submit_button = $('#ww-submit-button');
    let $hand_equal = $('.ww-hand-equal input');
    

    if( typeof wpcf7Elm == 'undefined' || !wpcf7Elm ) {
        return;
    }

    $units.change(function(){
        
        $patient_details.removeClass('ww-show');
        $patient_personalize.removeClass('ww-show');
        $subheader_to_hide.removeClass('ww-show');

        let $numbers_of_units = $(this).val();

        $patient_details.each(function(index){
            if( index < $numbers_of_units ){
                $(this).addClass('ww-show');
                $subheader_to_hide.addClass('ww-show')
            }
        })

        $patient_personalize.each(function(index){
            if( index < $numbers_of_units ){
                $(this).addClass('ww-show');
            }
        })
    })

    $submit_button.on('click', function(e){

        e.preventDefault();
        $('#ww-order-page-submit-hiden').trigger('submit');

    })

    $('.ww-patient-hand-site input').on('change', function(){

        $(this).parents('.ww-patient-content').find('.ww-hand-image-left').toggleClass('ww-hide');
        $(this).parents('.ww-patient-content').find('.ww-hand-image-right').toggleClass('ww-hide');
    })

    $('.ww-hand-equal input').on('click', function(){

        const $clicked_hand = $(this);
        let $hands_array = $('.ww-patient-personalize-container .ww-show');
        const $selected_color = $clicked_hand.parents('.ww-patient').find('.ww-patient-glove-color input[type="radio"]:checked').val();
        const $selected_site = $clicked_hand.parents('.ww-patient').find('.ww-patient-hand-site input[type="radio"]:checked').val();
        const $selected_gender = $clicked_hand.parents('.ww-patient').find('.ww-patient-used-before input[type="radio"]:checked').val();
        const $all_imputs = $('.ww-hand-properties :input, .ww-patient-glove-color :input');

        if($clicked_hand.prop('checked') === true){
            
            $hands_array.each(function(index){

                $(this).find('input[name="ww-hand-color-' + (index + 1) + '"]' ).each(function(){
                    if($(this).val() === $selected_color ) {
                        $(this).prop('checked',true);
                    }
                })

                $(this).find('input[name="ww-hand-site-' + (index + 1) + '"]' ).each(function(){
                    if($(this).val() === $selected_site ) {
                        $(this).prop('checked',true);
                    }
                })

                $(this).find('input[name="ww-hand-gender-' + (index + 1) + '"]' ).each(function(){
                    if($(this).val() === $selected_gender ) {
                        $(this).prop('checked',true);
                    }
                })
            })

            $all_imputs.prop('disabled',true);
            $all_imputs.parent().addClass('ww-disable-input');


        } else {
            $all_imputs.prop('disabled',false);
            $all_imputs.parent().removeClass('ww-disable-input');
        }
    })


    $('.ww-unit-select-arrow').on('click',function(){
        $('.ww-units-numner .ww-units-numbers').trigger('click');
    })

    $('.wpcf7').on('wpcf7mailsent',function(){
        $('.wpcf7-mail-sent-ok').prev().hide();
        $('#ww-submit-button').hide();
    })

    $('.ww-select-list li').on('click',function(){

        $clicked_list_el = $(this);

        if( !$clicked_list_el.hasClass('ww-after-black-background') ){

            $clicked_list_el.addClass('ww-after-black-background');
            $clicked_list_el.parent().prev().find('option[value=' + $clicked_list_el.text() + ']').attr('selected',true);
        } else {

            $clicked_list_el.removeClass('ww-after-black-background');
            $clicked_list_el.parent().prev().find('option[value=' + $clicked_list_el.text() + ']').attr('selected',false);
        }  
    })

    $('.ww-patient-glove-color .wpcf7-list-item').on('click',function(){
        if( !$(this).find('input').prop('disabled')){
            $(this).find('input').prop('checked',true);
        }
    })

})(jQuery);
