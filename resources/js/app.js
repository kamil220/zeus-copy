import './misc/accordions';
import './misc/toggle-sections';
import './misc/nav';
import './contact-form/thank-you-page';
import './contact-form/order-form';
import './carousels/swiper.min';
import './carousels/image-slider';
import './carousels/testimonials';

console.log( 'Web Wolf application loaded successfully ! Open the beer !' );

