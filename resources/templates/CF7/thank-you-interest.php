<section class="[ thank-message  thank-you-message ]  shadow--section  [ pb-3  px-3  pb-lg-5  px-lg-5 ]">
    <article class="[ d-flex  flex-column  align-items-center  justify-content-center ]  text--center  pb-lg-4">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/hand.png" alt="Aether bionic limb">
        <header class="thank-message__header">
            <h2 class="our-team__heading">
                <?=__( 'THANK YOU FOR EXPRESSING YOUR INTEREST', ZEUS_SLUG ); ?>
            </h2>
            <h3 class="our-team__subheading  line-height--normal  mb-2">
                <?=__( 'We received your request!', ZEUS_SLUG ); ?>
            </h3>
            <p class="thank-message__subheading">
                <?=__( 'Expect our response in 1-2 working days.', ZEUS_SLUG ); ?>
            </p>
        </header>
    </article>
</section>
