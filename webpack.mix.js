let mix = require('laravel-mix');

/**
 * Add application files
 */
mix.js('resources/js/app.js', 'public/js/');
/**
 * Add front-end stylesheets
 */
mix.sass('resources/scss/zeus.scss', 'public/css/').options({
    processCssUrls: false
});

/**
 * Add admin dashboard stylesheets
 */
mix.sass('resources/scss/zeus-admin.scss', 'public/css/').options({
    processCssUrls: false
});
