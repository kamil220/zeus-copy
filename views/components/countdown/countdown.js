function countdown(dateStr, displayElem, onTimerEnd = null) {
  let endDate = document.getElementById("endDate");

  endDate.textContent = getHeaderTime(dateStr);
  let targetDate = new Date(dateStr).getTime();
  let displaySymbols = [...displayElem.querySelectorAll(".symbol .cur")];
  let prevValue = "      ";
  let timer = setInterval(function() {
    let now = new Date().getTime();
    let remainingTime = targetDate - now;
    if (remainingTime <= 0) {
      clearInterval(timer); // stop timer
      if (onTimerEnd) {
        onTimerEnd(); // run callback
      }
    }

    let formattedTimestr = format(remainingTime);
    updateDisplay(formattedTimestr);
  }, 1000);

  function getHeaderTime(time) {
    const monthNames = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    ];

    let date = new Date(time);
    let stringDate =
      monthNames[date.getMonth()].toUpperCase() +
      " " +
      date.getDate() +
      "-" +
      date
        .getUTCFullYear()
        .toString()
        .slice(2, 4);

    return stringDate;
  }

  function format(time) {
    return [
      time / (60 * 60 * 24 * 1000), // days
      (time % 86400000) / 3600000, // hours
      (time % 3600000) / 60000, // minutes
      (time % 60000) / 1000
    ] // seconds
      .map(x =>
        Math.floor(x)
          .toString()
          .padStart(2, "0")
      )
      .join("");
  }

  function updateDisplay(timeString) {
    displaySymbols.forEach((curSymbol, i) => {
      let newValue = timeString[i];
      let currentValue = prevValue[i];
      if (currentValue !== newValue) {
        // animated change
        let parent = curSymbol.parentNode;
        parent.classList.remove("anim");
        curSymbol.textContent = newValue;
        curSymbol.nextElementSibling.textContent = currentValue;
        var foo = parent.offsetWidth; // reflow hack
        parent.classList.add("anim");
      }
    });
    prevValue = timeString;
  }
}

countdown("2020-03-25 00:00:00", document.getElementById("a"));
