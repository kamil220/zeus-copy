<style>
html, body{margin: 0px;background-color: #20262e;}

.timer {
  width: auto;
  margin: 10px auto 0px auto;
  font-family: Comic Sans MS;
  font-size: 36px;
  color:  #fff;
  cursor:  default;
  text-align:  center;
  line-height: 40px;
  border-radius: 10px;
  cursor:  default;
}

.display {
  align-items: center;
  justify-content: center;
  display: flex;
  height: 50px;
  height: 90px;
  margin: 0px auto 0px auto;
  color: #ffffff;
  padding: 10px 0px 10px 0px;
}

.symbol {
  width: 27px;
  height: 50px;
  float:  left;
  overflow:  hidden;
}
.symbol div {
}
.cur,.old{
  color: black;
  width: 92%;
  height: 40px;
  float: left;
  border-radius: 3px;
  margin: 5px 1px;
  transform: translateY(-50px);

}
.symbol.anim div {
  transform: translateY(0px);
  transition: transform .7s ease-in-out;
}
.delimeter {
  color: gray;
  width: 9px;
  float:  left;
  line-height: 47px;
  margin-left: 0px;
  text-indent: 0px;
}

.endDate{
	color: darkcyan;
}
</style>
<?php 
	wp_enqueue_script( 'script', get_stylesheet_directory_uri() . '/views/components/countdown/countdown.js', array ( 'jquery' ));
?>
<div class="timer" id="a">
<div>
  <h1 class="endDate" id="endDate">
  </h1>
</div>
	<div class="display">
  	<div class="symbol"><div class="cur"></div><div class="old"></div></div>
		<div class="symbol"><div class="cur"></div><div class="old"></div></div>
		<div class="delimeter">:</div>
		<div class="symbol"><div class="cur"></div><div class="old"></div></div>
		<div class="symbol"><div class="cur"></div><div class="old"></div></div>
		<div class="delimeter">:</div>
		<div class="symbol"><div class="cur"></div><div class="old"></div></div>
		<div class="symbol"><div class="cur"></div><div class="old"></div></div>
		<div class="delimeter">:</div>
		<div class="symbol"><div class="cur"></div><div class="old"></div></div>
		<div class="symbol"><div class="cur"></div><div class="old"></div></div>
	</div>
</div>
