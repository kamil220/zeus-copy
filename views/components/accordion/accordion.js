jQuery(function($) {
  $(document).ready(function() {
    $(".accordion-button").click(function(e) {
      var currentAttrvalue = $(this).attr("href");
      console.log(currentAttrvalue);
      if ($(e.target).is(".active")) {
        $(this).removeClass("active");
        $(".accordion-section-content:visible").slideUp(300);
      } else {
        $(".accordion-button")
          .removeClass("active")
          .filter(this)
          .addClass("active");
        $(".accordion-section-content")
          .slideUp(300)
          .filter(currentAttrvalue)
          .slideDown(300);
      }
    });
  });
});
