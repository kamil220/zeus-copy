

<style>
.accordion
{
    overflow:hidden;
    display:block;
    height: 15vh;
    border-radius:4px;
    background: white;
}

.accordion-section{
    display: flex;
    align-items: center;
    width: 100%;
    padding-left: 10%;
    padding-right: 10%;
}

.accordion-button{
    height: 20%;
    width: 45%;
    display: flex;
}

.accordion-section-title
{
    width:100%;
    padding:15px;
}
.accordion-section-title
{
        width: 100%;
        padding: 15px;
        display: inline-block;
        background-color: white;
        font-size: 1.2em;
        color: black;
        transition: all linear 0.5s;
        text-decoration:none;
}
.accordion-section-title.active
{
    background-color:white;
    text-decoration:none;
    }
    .accordion-section-title:hover
    {
    background-color: white;
    text-decoration:none;
}
.accordion-section:last-child .accordion-section-title
{
    border-bottom:none;
}
.accordion-section-content
{
    padding:15px;
    display:none;
}
</style>

<?php 
	wp_enqueue_script( 'accordionScript', get_stylesheet_directory_uri() . '/views/components/accordion/accordion.js', array ( 'jquery' ));
?>

 <div class="accordion">
        <div class="accordion-section">
            <label class="accordion-section-title" href="#accordion-1">Accordion section #1</label>
            <button href="#accordion-1" class="[ btn  btn--gradient  btn--gradient-info  [ btn--icon  btn--icon-right ] ] accordion-button">
                Read more
                <span class="icon">
                    <i class="fas fa-chevron-circle-right"></i>
                </span>
            </button>
            <!-- W divie poniżej dodać pehapem kolor tła by zgadzał się z mockupami -->
           
        </div>
            <div id="accordion-1" class="accordion-section-content"> 
                <p>This is first accordion section</p>
            </div>
    </div>