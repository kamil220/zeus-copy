<?php
$item = get_query_var('item');
?>

<article class="timeline__article">
    <div class="timeline__article-icon">
        <!-- editable icon -->
        <?php echo $item['icon'] ?>
    </div>
    <div class="timeline__article-content">
        <div class="timeline__article-line">
            <!-- pin color here -->
            <span class="pin" style="background: <?php echo $item['color'] ?>"></span>
            <strong class="year">
                <!-- editable year -->
                <?php echo $item['year'] ?>
            </strong>
        </div>
        <div class="content">
            <!-- editable text -->
            <?php echo $item['content'] ?>
        </div>
    </div>
</article>