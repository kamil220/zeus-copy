<?php
    /* Get Timeline instance */
    $timelineHandler = WW\Services\Timeline\Models\Timeline::instance();
    /* Get Timeline Items */
    $timelineItems = $timelineHandler->getTimelineItems();
    
    $upItems = [];
    $downItems = [];
    $counter = 0;

    foreach( $timelineItems as $item ){
        
        if ( $counter%2 == 0 ) {
            array_push( $upItems, $timelineHandler->getTimelineItem( $item ) );
        } else {
            array_push( $downItems, $timelineHandler->getTimelineItem($item) );
        }
        $counter++;

    }

    $counter = 0;
    error_log(print_r($downItems,true));
?>

<section class="timeline">
    <div class="[ timeline__row  timeline__upper ]">
        <!-- forearch the whole upper article -->
        <?php 
            foreach( $upItems as $timeline_item ){
                set_query_var( 'item', $timeline_item );
                echo get_template_part('views/components/timeline/article');
            }
        ?>
    </div>
    <div class="[ timeline__row  timeline__lower ]">
        <!-- forearch the whole lower article -->
        <?php 
            foreach( $downItems as $timeline_item ){
                set_query_var( 'item', $timeline_item );
                echo get_template_part('views/components/timeline/article');
            }
        ?>
    </div>
</section>