<article class="d-flex  flex-column  align-items-start">
    <button class="[ btn  btn--gradient  btn--gradient-info  [ btn--icon  btn--icon-right ] ]">
        Btn info - right icon
        <span class="icon">
            <i class="fas fa-chevron-circle-right"></i>
        </span>
    </button>
    <button class="[ btn  btn--gradient  btn--gradient-success  [ btn--icon  btn--icon-right ] ]">
        Btn success - right icon
        <span class="icon">
            <i class="fas fa-chevron-circle-right"></i>
        </span>
    </button>

    <button class="[ btn  btn--gradient  btn--gradient-warning  [ btn--icon  btn--icon-right ] ]">
        Btn warning - right icon
        <span class="icon">
            <i class="fas fa-chevron-circle-right"></i>
        </span>
    </button>

    <button class="[ btn  btn--gradient  btn--gradient-pink  [ btn--icon  btn--icon-left ] ]">
        Btn pink - left icon
        <span class="icon">
            <i class="fas fa-chevron-circle-right"></i>
        </span>
    </button>

    <button class="[ btn  btn--gradient  btn--gradient-violet  [ btn--icon  btn--icon-left ] ]">
        Btn violet - left icon
        <span class="icon">
            <i class="fas fa-chevron-circle-right"></i>
        </span>
    </button>
</article>