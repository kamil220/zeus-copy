<?php
/* Get employee handler */
$employeeHandler = WW\Services\OurTeam\Models\Employee::instance();
$employee_id = get_query_var( 'employee_id' );
$employee = $employeeHandler->getEmployee( $employee_id );

?>
<article class="person-article  [ d-flex  flex-column  justify-content-center  align-items-center ]">
    <figure class="person-article__img  position-relative">
        <?php
            echo wp_get_attachment_image( $employee['photo_id'], 'post-thumbnail' );
        ?>
        <figcaption class="position-absolute  py-4  [ d-flex  flex-column  justify-content-end  align-items-center ]">
            <strong class="name">
                <?=$employee['name'];?>
            </strong>
            <div class="btn--group  d-flex  py-2">
                <a href="mailto:<?=$employee['email'];?>" class="[ icon  icon--circled ]  text--white  email  mx-1"
                    title="<?=$employee['email'];?>">
                    <i class="fas fa-envelope"></i>
                </a>
                <?php if( !empty( $employee['linkedin'] ) ): ?>
                    <a href="<?=$employee['linkedin'];?>" class="[ icon  icon--circled ]  text--white  linkedin  mx-1" title="LinkedIn">
                        <i class="fab fa-linkedin-in"></i>
                    </a>
                <?php endif; ?>
            </div>
        </figcaption>
    </figure>
    <div class="person-article__info  [ d-flex  flex-column  justify-content-center  align-items-center ]  [ mt-3  mt-md-4 ]">
        <h4 class="name">
            <?=$employee['name'];?>
        </h4>
        <span class="position">
            <?=$employee['job_title'];?>
        </span>
        <a href="mailto:<?=$employee['email'];?>" class="email  mt-1" title="<?=$employee['email'];?>">
            <?=$employee['email'];?>
        </a>
    </div>
</article>
