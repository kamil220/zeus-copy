<?php
/* Get employee handler */
$employeeHandler = WW\Services\OurTeam\Models\Employee::instance();

/* Define amount of featured employee */
$amount_of_featured = apply_filters( 'WW/Services/OurTeam/Employees/AmountOfFeatured', 2 );

?>

<section class="our-team  text--center  [ py-4  py-md-5 ]">
    <div class="container">
        <header class="[ mb-4  mb-md-5 ]">
            <h2 class="our-team__heading">
                <?= __( 'Let\'s meet', ZEUS_SLUG ); ?>
            </h2>
            <h3 class="our-team__subheading">
                <?= __( 'Meet our team!', ZEUS_SLUG ); ?>
            </h3>
        </header>

        <!-- Featured articles -->
        <ul class="[ our-team__articles  our-team__articles--featured ]  row  align-items-center  justify-content-center">
            <?php
                /* Get employees */
                $featured_employees = $employeeHandler->getEmployees( ['posts_per_page' => $amount_of_featured]);

                foreach ( $featured_employees as $employee ) {
                    set_query_var( 'employee_id', $employee );
                    ?>
                    <li class="col-12  col-md-6">
                        <?php get_template_part('views/components/about/single-person'); ?>
                    </li>
                    <?php
                }
            ?>
        </ul>

        <!-- Other articles -->
        <ul class="[ our-team__articles ]  row  align-items-center  justify-content-around">
            <?php
                /*
                 *  Get employees
                 *  Offset not working with posts_per_page = -1
                 */
                $employees = $employeeHandler->getEmployees( [ 'posts_per_page' => '100', 'offset' => $amount_of_featured ] );

                foreach ( $employees as $employee ) {
                    set_query_var( 'employee_id', $employee );
                    ?>
                    <li class="col-12  col-md-6  col-lg-4">
                        <?php get_template_part('views/components/about/single-person'); ?>
                    </li>
                    <?php
                }
            ?>
        </ul>

    </div>
</section>
