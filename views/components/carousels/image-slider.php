<section class="image-slider">
    <!-- data-speed = Speed in ms in -->
    <!-- data-autoplay = autoplay delay in ms -->
    <!-- data-arrows = enable or disable arrows = true or false -->
    <!-- data-direction = false = slide-right, true = slide-left -->
    <!-- data-pauseonhover = if true, then pause the autoplay -->
    <div class="swiper-container  image-slider__swiper" data-speed="300" data-autoplay="1500" data-arrows="true"
        data-direction="true" data-pauseonhover="true">
        <!-- Additional required wrapper -->
        <div class="swiper-wrapper">
            <figure class="swiper-slide">
                <img src="https://via.placeholder.com/400x200" alt="">
            </figure>
            <figure class="swiper-slide">
                <img src="https://via.placeholder.com/400x200" alt="">
            </figure>
            <figure class="swiper-slide">
                <img src="https://via.placeholder.com/400x200" alt="">
            </figure>
            <figure class="swiper-slide">
                <img src="https://via.placeholder.com/400x200" alt="">
            </figure>
            <figure class="swiper-slide">
                <img src="https://via.placeholder.com/400x200" alt="">
            </figure>
            <figure class="swiper-slide">
                <img src="https://via.placeholder.com/400x200" alt="">
            </figure>
            <figure class="swiper-slide">
                <img src="https://via.placeholder.com/400x200" alt="">
            </figure>
            <figure class="swiper-slide">
                <img src="https://via.placeholder.com/400x200" alt="">
            </figure>
        </div>

        <!-- If we need navigation buttons -->
        <div class="[ swiper-button  swiper-button--absolute  swiper-button-prev ]">
            <button class="[ btn  btn--icon-only ]">
                <span class="[ icon  icon--adaptive ]">
                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="94" viewBox="0 0 30 94">
                        <defs>
                            <clipPath id="9bf1a">
                                <path fill="#fff"
                                    d="M2.114 48.464V1.536h26.772v46.928zm1.443-1.695l.886.462 23-44-.886-.463z" />
                            </clipPath>
                            <clipPath id="9bf1b">
                                <path fill="#fff"
                                    d="M1.228 93.773V44.227h28.544v49.546zm3.215-47.004l-.886.462 23 44 .886-.463z" />
                            </clipPath>
                        </defs>
                        <g>
                            <g>
                                <path fill="#fff" d="M3.557 46.769l.886.462 23-44-.886-.463z" />
                                <path fill="none" stroke="#fff" stroke-miterlimit="20" stroke-width="4"
                                    d="M3.557 46.769v0l.886.462v0l23-44v0l-.886-.463v0z"
                                    clip-path="url(&quot;#9bf1a&quot;)" />
                            </g>
                            <g>
                                <path fill="#fff" d="M27.443 90.768l-.886.464-23-44 .886-.463z" />
                                <path fill="none" stroke="#fff" stroke-miterlimit="20" stroke-width="4"
                                    d="M27.443 90.768v0l-.886.464v0l-23-44v0l.886-.463v0z"
                                    clip-path="url(&quot;#9bf1b&quot;)" />
                            </g>
                        </g>
                    </svg>
                </span>
            </button>
        </div>
        <div class="[ swiper-button  swiper-button--absolute  swiper-button-next ]">
            <button class="[ btn  btn--icon-only ]">
                <span class="[ icon  icon--adaptive ]">
                    <svg xmlns="http://www.w3.org/2000/svg" width="31" height="94" viewBox="0 0 31 94">
                        <defs>
                            <clipPath id="qv6wa">
                                <path fill="#fff"
                                    d="M2.114 92.464V45.536h26.772v46.928zM27.443 47.23l-.886-.462-23 44 .886.463z" />
                            </clipPath>
                            <clipPath id="qv6wb">
                                <path fill="#fff"
                                    d="M1.228 49.773V.227h28.544v49.546zm25.329-2.542l.886-.462-23-44-.886.462z" />
                            </clipPath>
                        </defs>
                        <g>
                            <g>
                                <path fill="#fff" d="M27.443 47.231l-.886-.462-23 44 .886.463z" />
                                <path fill="none" stroke="#fff" stroke-miterlimit="20" stroke-width="4"
                                    d="M27.443 47.231v0l-.886-.462v0l-23 44v0l.886.463v0z"
                                    clip-path="url(&quot;#qv6wa&quot;)" />
                            </g>
                            <g>
                                <path fill="#fff" d="M3.557 3.231l.886-.463 23 44-.886.463z" />
                                <path fill="none" stroke="#fff" stroke-miterlimit="20" stroke-width="4"
                                    d="M3.557 3.231v0l.886-.463v0l23 44v0l-.886.463v0z"
                                    clip-path="url(&quot;#qv6wb&quot;)" />
                            </g>
                        </g>
                    </svg>
                </span>
            </button>
        </div>
    </div>
</section>
