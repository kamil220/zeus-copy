<?php
/* Get employee handler */
$testimonialsHandler = WW\Services\Testimonials\Models\Testimonial::instance();
$tag = 'ww_testimonials';
$background_attachment_id = get_query_var( $tag . '_attachment_id' );
$background_attachment_url = $background_attachment_id ? wp_get_attachment_image_url( $background_attachment_id, 'full' ) : '';
/* Get testimonials */
$testimonials = $testimonialsHandler->getTestimonials();

?>

<!-- put inline background image from an ACF field here-->
<section class="testimonials  position-relative  [ py-4  py-md-5 ]"
    style="background: url('<?=$background_attachment_url;?>') no-repeat center / cover">

    <div class="container  py-md-2  [ text--white  text--center ]">
        <div class="row  justify-content-center">
            <!-- logo image goes here -->
            <div class="[ col-8  col-lg-5 ]  testimonials__header  [ d-flex  justify-content-center ] pb-5  mb-md-4">
                <div class="swiper-container  testimonials__header-swiper">
                    <div class="swiper-wrapper">
                        <?php
                            foreach( $testimonials as $testimonial_id ) {
                                $testimonial = $testimonialsHandler->getTestimonial( $testimonial_id );
                                ?>
                                <div class="swiper-slide  [ d-flex  justify-content-center  align-items-center ]">
                                    <?=wp_get_attachment_image( $testimonial['brand_logo'], [ 360, 60 ] ); ?>
                                </div>
                                <?php
                            }
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-12  p-0"></div>
            <!-- prev arrow -->
            <div
                class="ww-swiper-button-prev  [ d-none  d-sm-flex ]  col-2  [ pr-0  pl-md-0 ]  [ justify-content-start  justify-content-lg-center  align-items-center ]">
                <button class="[ btn  btn--icon-only ]">
                    <span class="[ icon  icon--adaptive ]">
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="94" viewBox="0 0 30 94">
                            <defs>
                                <clipPath id="9bf1a">
                                    <path fill="#fff"
                                        d="M2.114 48.464V1.536h26.772v46.928zm1.443-1.695l.886.462 23-44-.886-.463z" />
                                </clipPath>
                                <clipPath id="9bf1b">
                                    <path fill="#fff"
                                        d="M1.228 93.773V44.227h28.544v49.546zm3.215-47.004l-.886.462 23 44 .886-.463z" />
                                </clipPath>
                            </defs>
                            <g>
                                <g>
                                    <path fill="#fff" d="M3.557 46.769l.886.462 23-44-.886-.463z" />
                                    <path fill="none" stroke="#fff" stroke-miterlimit="20" stroke-width="4"
                                        d="M3.557 46.769v0l.886.462v0l23-44v0l-.886-.463v0z"
                                        clip-path="url(&quot;#9bf1a&quot;)" />
                                </g>
                                <g>
                                    <path fill="#fff" d="M27.443 90.768l-.886.464-23-44 .886-.463z" />
                                    <path fill="none" stroke="#fff" stroke-miterlimit="20" stroke-width="4"
                                        d="M27.443 90.768v0l-.886.464v0l-23-44v0l.886-.463v0z"
                                        clip-path="url(&quot;#9bf1b&quot;)" />
                                </g>
                            </g>
                        </svg>
                    </span>
                </button>
            </div>
            <div class="swiper-container  testimonials__swiper  [ col-10  col-sm-8 ]  [ d-flex  flex-wrap  align-items-center ]">
                <div class="swiper-wrapper">
                    <!--  Display slides in foreach loop here  -->
                    <?php
                        foreach( $testimonials as $testimonial_id ) {
                            $testimonial = $testimonialsHandler->getTestimonial( $testimonial_id );
                            ?>
                    <div class="swiper-slide">
                        <?=$testimonial['review'];?>
                    </div>
                    <?php
                        }
                    ?>
                </div>
            </div>
            <!-- next arrow -->
            <div
                class="ww-swiper-button-next  [ d-none  d-sm-flex ]  col-2  [ pl-0  pr-md-0 ]  [ justify-content-end  justify-content-lg-center  align-items-center ]">
                <button class="[ btn  btn--icon-only ]">
                    <span class="[ icon  icon--adaptive ]">
                        <svg xmlns="http://www.w3.org/2000/svg" width="31" height="94" viewBox="0 0 31 94">
                            <defs>
                                <clipPath id="qv6wa">
                                    <path fill="#fff"
                                        d="M2.114 92.464V45.536h26.772v46.928zM27.443 47.23l-.886-.462-23 44 .886.463z" />
                                </clipPath>
                                <clipPath id="qv6wb">
                                    <path fill="#fff"
                                        d="M1.228 49.773V.227h28.544v49.546zm25.329-2.542l.886-.462-23-44-.886.462z" />
                                </clipPath>
                            </defs>
                            <g>
                                <g>
                                    <path fill="#fff" d="M27.443 47.231l-.886-.462-23 44 .886.463z" />
                                    <path fill="none" stroke="#fff" stroke-miterlimit="20" stroke-width="4"
                                        d="M27.443 47.231v0l-.886-.462v0l-23 44v0l.886.463v0z"
                                        clip-path="url(&quot;#qv6wa&quot;)" />
                                </g>
                                <g>
                                    <path fill="#fff" d="M3.557 3.231l.886-.463 23 44-.886.463z" />
                                    <path fill="none" stroke="#fff" stroke-miterlimit="20" stroke-width="4"
                                        d="M3.557 3.231v0l.886-.463v0l23 44v0l-.886.463v0z"
                                        clip-path="url(&quot;#qv6wb&quot;)" />
                                </g>
                            </g>
                        </svg>
                    </span>
                </button>
            </div>

            <!-- Persons are here -->
            <div
                class="testimonials__pagination  testimonials__persons  swiper-pagination  col-12  [ mt-4  mt-md-5 ]  pt-lg-2">
                <div class="row">
                    <div class="col-2  [ d-none  d-md-block ]"></div>
                    <?php
                    $i = -1;
                    foreach( $testimonials as $testimonial_id ) {
                        $testimonial = $testimonialsHandler->getTestimonial( $testimonial_id );

                        $i++;
                        ?>
                    <figure class="testimonials__person  px-0  [ col-6  col-md-4 ]  [ d-flex  justify-content-center ]">
                        <div class="testimonials__person--inner  [ d-flex  flex-column  align-items-center  justify-content-between  flex-md-row ]  <?php if($i === 0) echo 'active'; ?>"
                            data-slide-index="<?php echo $i ?>">
                            <!-- place person image & details here -->
                            <div class="testimonials__person--img  [ mb-2  mb-md-0  mr-md-2  mr-lg-3 ]">
                                <?= wp_get_attachment_image( $testimonial['person_picture'], 'post-thumbnail' ) ?>
                            </div>
                            <figcaption class="testimonials__person--details  text-md--left">
                                <strong class="d-block">
                                    <?=$testimonial['name'];?>
                                </strong>
                                <p>
                                    <?=$testimonial['signature'];?>
                                </p>
                            </figcaption>
                        </div>
                    </figure>
                    <?php
                    }
                    ?>
                    <div class="[ col-2  ]  [ d-none  d-md-block ]"></div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--<script src="https://unpkg.com/swiper/js/swiper.min.js"></script>-->
