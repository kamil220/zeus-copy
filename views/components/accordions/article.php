<?php
    $shortcode = WW\Services\Accordions\Controllers\Shortcode::instance();
    $shortcode_tag = $shortcode->getTag();

    $title = get_query_var( $shortcode_tag . '_title' );
    $button_title = get_query_var( $shortcode_tag . '_button-title' );
    $button_url = get_query_var( $shortcode_tag . '_button-url' );
    $button_type = get_query_var( $shortcode_tag . '_button-type' );
    $content = get_query_var( $shortcode_tag . '_content' );
?>
<!-- Single ww-accordion -->
<article class="ww-accordion  col-12">
    <!-- remove class "read-more" if you want it to be a link -->
    <div class="row  [ ww-accordion__trigger  ww-accordion--shadow ]  <?= $button_type == 'accordion' ? 'read-more' : ''; ?>">

        <div class="ww-accordion__text  [ d-flex  flex-column  justify-content-center  align-items-start ]  col-6  py-4">
            <!-- Left side ww-accordion text -->
            <?=$title;?>
        </div>
        <div
            class="ww-accordion__button  [ d-flex  flex-column  justify-content-center  align-items-end ]  col-6  py-4">

            <!-- Beggining of the elementor button, you can put the elementor button below -->
            <div class="elementor-element  elementor-widget  elementor-widget-button">
                <div class="elementor-widget-container">
                    <div class="elementor-button-wrapper">

                    <!-- If "read-more" class is on, it doesn't matter if the button is a link or not, if we don't have "read-more" class, then we should have a link to some attachment here -->
                        <a href="<?=$button_type == 'url' ? $button_url : '#'; ?>" class="elementor-button">
                            <span class="elementor-button-content-wrapper">
                                <span class="elementor-button-icon elementor-align-icon-right">
                                    <i aria-hidden="true" class="fas fa-arrow-alt-circle-right"></i> </span>

                                <!-- Or You can use text only & button will stay the same -->
                                <span class="elementor-button-text"><?=$button_title;?></span>

                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <!-- End of elementor button -->

        </div>
    </div>
    <!-- ww-accordion content after opening -->
    <div class="ww-accordion__content  row  bg--white  mt-0">
        <div class="col-12  [ py-4  pb-lg-5 ]">
            <?=$content;?>
        </div>
    </div>
    <!-- End of ww-accordion content -->
</article>
