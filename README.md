# **Commands**

```
npm run dev
```
build your application

```
npm run watch
```
watch your application

# **Directory Structure**

``app`` - application folder

``assets`` - folder for fonts, images etc.

``config``  - directory with configs

``public``  - public directory which compiled files

``resources``   - work directory with scss and js 

# Used technology
- https://laravel-mix.com/docs/5.0/installation
- https://wpastra.com/pro/
- https://www.advancedcustomfields.com/pro/
