<?php

define( 'DIST_PATH', get_stylesheet_directory() . '/resources/' );
define( 'DIST_URL', get_stylesheet_directory_uri() . '/resources/' );

define( 'APP_PATH', get_stylesheet_directory() . '/app/' );
define( 'APP_URL', get_stylesheet_directory_uri() . '/app/' );

define( 'PUBLIC_PATH', get_stylesheet_directory() . '/public/' );
define( 'PUBLIC_URL', get_stylesheet_directory_uri() . '/public/' );

define( 'VIEWS_PATH', get_stylesheet_directory() . '/views/' );
define( 'VIEWS_URL', get_stylesheet_directory_uri() . '/views/' );

define( 'COMPONENTS_PATH', VIEWS_PATH . '/components/' );
define( 'COMPONENTS_URL', VIEWS_URL . '/components/' );

define( 'SERVICES_PATH', APP_PATH . '/Services/' );
define( 'SERVICES_URL', APP_URL . '/Services/' );

define( 'HELPERS_PATH', APP_PATH . '/Helpers/' );
define( 'HELPERS_URL', APP_URL . '/Helpers/' );

define( 'TEMPLATES_PATH', DIST_PATH . '/templates/' );
define( 'TEMPLATES_URL', DIST_URL . '/templates/' );



define( 'ZEUS_SLUG', 'WW_ZEUS' );
define( 'ZEUS_VERSION', '1.0.0' );
