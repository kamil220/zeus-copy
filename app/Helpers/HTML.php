<?php

namespace WW\Helpers;

class HTML
{
    public static function getFromFile( $path ) {
        ob_start();
        include $path;
        return ob_get_clean();
    }
}
