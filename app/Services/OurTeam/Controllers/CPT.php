<?php


namespace WW\Services\OurTeam\Controllers;


class CPT
{
    /**
     * @var $instance ServiceHandler
     */
    private static $instance;

    /**
     * ServiceHandler constructor.
     */
    private function __construct()
    {
        add_action( 'init', [ $this, 'initialize' ] );

        if( !function_exists('acf_add_local_field_group') ) {
            return;
        }

        $this->settings();
    }

    /**
     * @return ServiceHandler
     * @author Kamil Łazarz
     */
    public static function instance() {
        if( self::$instance === null ) {
            self::$instance = new CPT;
        }

        return self::$instance;
    }

    /**
     * Initialize ACF methods
     * @author Kamil Łazarz
     */
    public function initialize()
    {
        $labels = array(
            'name'                  => _x( 'Employees', 'Post Type General Name', 'ZEUS_THEME' ),
            'singular_name'         => _x( 'Employee', 'Post Type Singular Name', 'ZEUS_THEME' ),
            'menu_name'             => __( 'Employees', 'ZEUS_THEME' ),
            'name_admin_bar'        => __( 'Employee', 'ZEUS_THEME' ),
            'archives'              => __( 'Item Archives', 'ZEUS_THEME' ),
            'attributes'            => __( 'Item Attributes', 'ZEUS_THEME' ),
            'parent_item_colon'     => __( 'Parent Item:', 'ZEUS_THEME' ),
            'all_items'             => __( 'All Items', 'ZEUS_THEME' ),
            'add_new_item'          => __( 'Add New Item', 'ZEUS_THEME' ),
            'add_new'               => __( 'Add New', 'ZEUS_THEME' ),
            'new_item'              => __( 'New Item', 'ZEUS_THEME' ),
            'edit_item'             => __( 'Edit Item', 'ZEUS_THEME' ),
            'update_item'           => __( 'Update Item', 'ZEUS_THEME' ),
            'view_item'             => __( 'View Item', 'ZEUS_THEME' ),
            'view_items'            => __( 'View Items', 'ZEUS_THEME' ),
            'search_items'          => __( 'Search Item', 'ZEUS_THEME' ),
            'not_found'             => __( 'Not found', 'ZEUS_THEME' ),
            'not_found_in_trash'    => __( 'Not found in Trash', 'ZEUS_THEME' ),
            'featured_image'        => __( 'Featured Image', 'ZEUS_THEME' ),
            'set_featured_image'    => __( 'Set featured image', 'ZEUS_THEME' ),
            'remove_featured_image' => __( 'Remove featured image', 'ZEUS_THEME' ),
            'use_featured_image'    => __( 'Use as featured image', 'ZEUS_THEME' ),
            'insert_into_item'      => __( 'Insert into item', 'ZEUS_THEME' ),
            'uploaded_to_this_item' => __( 'Uploaded to this item', 'ZEUS_THEME' ),
            'items_list'            => __( 'Items list', 'ZEUS_THEME' ),
            'items_list_navigation' => __( 'Items list navigation', 'ZEUS_THEME' ),
            'filter_items_list'     => __( 'Filter items list', 'ZEUS_THEME' ),
        );

        $args = array(
            'label'                 => __( 'Employee', 'ZEUS_THEME' ),
            'labels'                => $labels,
            'supports'              => [ 'title' ],
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 10,
            'menu_icon'             => 'dashicons-groups',
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'capability_type'       => 'page',
        );
        register_post_type( 'ww_employees', $args );
    }

    public function settings() {
        acf_add_local_field_group(array(
            'key' => 'group_5e594b032984',
            'title' => 'Employee details',
            'fields' => array(
                array(
                    'key' => 'field_5e594b81064e1',
                    'label' => 'Photo',
                    'name' => 'ww_photo',
                    'type' => 'image',
                    'instructions' => '',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'return_format' => 'id',
                    'preview_size' => 'medium',
                    'library' => 'all',
                    'min_width' => 150,
                    'min_height' => 150,
                    'min_size' => '',
                    'max_width' => 700,
                    'max_height' => 700,
                    'max_size' => '',
                    'mime_types' => '',
                ),
                array(
                    'key' => 'field_5e594bf8064e3',
                    'label' => 'Job title',
                    'name' => 'ww_job_title',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_5e594bcb064e2',
                    'label' => 'E-Mail',
                    'name' => 'ww_email',
                    'type' => 'email',
                    'instructions' => '',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                ),
                array(
                    'key' => 'field_5e594ccf064e7',
                    'label' => 'Linkedin',
                    'name' => 'ww_linkedin',
                    'type' => 'url',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                ),
            ),
            'location' => array(
                array(
                    array(
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'ww_employees',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => array(
                0 => 'permalink',
                1 => 'the_content',
                2 => 'excerpt',
                3 => 'discussion',
                4 => 'comments',
                5 => 'revisions',
                6 => 'slug',
                7 => 'author',
                8 => 'format',
                9 => 'page_attributes',
                10 => 'featured_image',
                11 => 'categories',
                12 => 'tags',
                13 => 'send-trackbacks',
            ),
            'active' => true,
            'description' => '',
        ));
    }
}
