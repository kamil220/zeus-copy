<?php


namespace WW\Services\OurTeam\Controllers;


use WW\Helpers\HTML;

class Shortcode
{

    public $tag = 'ww_employees';

    /**
     * @var $instance Shortcode
     */
    private static $instance;

    /**
     * Shortcode constructor.
     */
    private function __construct()
    {
        add_action( 'init', [ $this, 'initialize' ] );
    }

    /**
     * @return Shortcode
     * @author Kamil Łazarz
     */
    public static function instance() {
        if( self::$instance === null ) {
            self::$instance = new Shortcode();
        }

        return self::$instance;
    }

    /**
     * Add shortcode
     * @author Kamil Łazarz
     */
    public function initialize()
    {
        add_shortcode( $this->tag, [ $this, 'callback' ] );
    }

    /**
     * Return shortcode content
     * @author Kamil Łazarz
     * @return false|string|HTML
     */
    public function callback() {
        return HTML::getFromFile( COMPONENTS_PATH . 'about/our-team.php' );
    }

    /**
     * Get shortcode tag
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }
}
