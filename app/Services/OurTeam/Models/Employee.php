<?php


namespace WW\Services\OurTeam\Models;

class Employee
{
    /**
     * @var $instance Employee
     */
    private static $instance;
    private $post_type = 'ww_employees';

    /**
     * ServiceHandler constructor.
     */
    private function __construct()
    {
       // silence si gold
    }

    /**
     * @return Employee
     * @author Kamil Łazarz
     */
    public static function instance() {
        if( self::$instance === null ) {
            self::$instance = new Employee();
        }

        return self::$instance;
    }

    /**
     * @param array $args
     * @return int[]|WP_Post[]
     * @author Kamil Łazarz
     */
    public function getEmployees( $args = [] ) {

        $args = wp_parse_args(
            $args,
            [
                'post_type' => $this->post_type,
                'posts_per_page' => -1,
                'fields'    => 'ids',
                'order' => 'ASC',
            ] );

        $employees = get_posts( $args );

        return $employees;
    }

    /**
     * GET Employee details
     * @author Kamil Łazarz
     * @param $id
     * @return array
     */
    public function getEmployee( $id ) {

        $data = [
            'name'  => get_the_title( $id),
            'photo_id' => get_post_meta( $id, 'ww_photo', true ),
            'job_title' => get_post_meta( $id, 'ww_job_title', true ),
            'email' => get_post_meta( $id, 'ww_email', true ),
            'linkedin' => get_post_meta( $id, 'ww_linkedin', true )
        ];

        return $data;
    }
}
