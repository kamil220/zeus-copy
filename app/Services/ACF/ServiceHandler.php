<?php


namespace WW\Services\ACF;

/**
 * Class ServiceHandler
 * @package WW\Services\ACF
 * @author Kamil Łazarz
 */
class ServiceHandler
{
    /**
     * @var $instance ServiceHandler
     */
    private static $instance;

    /**
     * ServiceHandler constructor.
     */
    private function __construct()
    {
        $this->initialize();
    }

    /**
     * @return ServiceHandler
     * @author Kamil Łazarz
     */
    public static function instance() {
        if( self::$instance === null ) {
            self::$instance = new ServiceHandler;
        }

        return self::$instance;
    }

    /**
     * Initialize ACF methods
     * @author Kamil Łazarz
     */
    private function initialize()
    {

        $this->settings();

        /* Include plugin only if there are not added before  */
        if( function_exists( 'get_field' ) ) {
            return;
        }

        $this->includePlugin();
    }

    /**
     * Show or hide Advance Custom Field in admin dashboard
     *
     * @author Kamil Łazarz
     */
    private function settings() {
        if( DEV_MODE || !ACF_HIDE ) {
            add_filter('acf/settings/show_admin', '__return_true', 999 );
        } else {
            add_filter('acf/settings/show_admin', '__return_false', 999 );
        }

    }

    /**
     * Include Advance Custom Field Pro and setup plugin Path and Dir
     * @author Kamil Łazarz
     */
    private function includePlugin() {

        require_once SERVICES_PATH . 'ACF/advanced-custom-fields-pro/acf.php';

        /* ACF Path */
        add_filter('acf/settings/path', [ $this, 'setupPath' ] );

        /* ACF DIR */
        add_filter('acf/settings/dir', [ $this, 'setupDir' ] );
    }

    /**
     * @param $path string absolute path to advance custom field pro directory
     * @return string
     * @author Kamil Łazarz
     */
    public function setupPath($path) {
        return SERVICES_PATH . 'ACF/advanced-custom-fields-pro/';
    }

    /**
     * @param $dir string URL to advance custom field pro directory
     * @return string
     * @author Kamil Łazarz
     */
    public function setupDir($dir ) {
        return SERVICES_URL . 'ACF/advanced-custom-fields-pro/';
    }
}
