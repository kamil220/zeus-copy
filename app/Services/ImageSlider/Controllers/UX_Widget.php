<?php


namespace WW\Services\ImageSlider\Controllers;
use Elementor\Widget_Base;

class UX_Widget extends Widget_Base
{
    /**
     * Get widget name.
     */
    public function get_name()
    {
        return 'ww_imageslider';
    }

    /**
     * Get widget title.
     */
    public function get_title()
    {
        return __('Image Slider', ZEUS_SLUG );
    }

    /**
     * Get widget icon.
     */
    public function get_icon()
    {
        return 'fas fa-users ww-marker';
    }

    /**
     * Get widget categories.
     */
    public function get_categories()
    {
        return [ 'general', 'webwolf' ];
    }

    /**
     * Set elementor controls
     */

    public function _register_controls() {

        $this->start_controls_section(
			'content_section',
			[
				'label' => __( 'Content', 'webwolf' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

        /* Adding component control */
        /* Image Gallery */
		$this->add_control(
			'gallery',
			[
				'label' => __( 'Add Images', 'webwolf' ),
				'type' => \Elementor\Controls_Manager::GALLERY,
				'default' => [],
			]
        );
        
        /* Stop on hover */
        $this->add_control(
			'stop_on_hover',
			[
				'label' => __( 'Stop on hover', 'webwolf' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'On', 'webwolf' ),
				'label_off' => __( 'Off', 'webwolf' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);
        
        /* Moving direction */
        $this->add_control(
			'moving_direction',
			[
				'label' => __( 'Auto moving direction, default right', 'webwolf' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Right', 'webwolf' ),
				'label_off' => __( 'Left', 'webwolf' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
        );

        /* Animation Speed */        
		$this->add_control(
			'animation_speed',
			[
				'label' => __( 'Animation speed', 'webwolf' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'min' => 100,
				'max' => 5000,
				'step' => 100,
				'default' => 400,
			]
        );
        
        /* Add arrows */
        $this->add_control(
            'add_arrows',
            [
                'label' => __( 'Add arrows', 'webwolf' ),
                'type' => \Elementor\Controls_Manager::SWITCHER,
                'label_on' => __( 'On', 'webwolf' ),
                'label_off' => __( 'Off', 'webwolf' ),
                'return_value' => 'yes',
                'default' => 'yes',
            ]
        );

        /* Slider Height */        
        $this->add_control(
			'height',
			[
				'label' => __( 'Height', 'webwolf' ),
				'type' => \Elementor\Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 1000,
					],
				],
			]
		);

		$this->end_controls_section();
    }

    /**
     * Render shortcode
     */
    protected function render()
    {
        $shortcode = Shortcode::instance();
        error_log(print_r($shortcode,true));
        echo do_shortcode( sprintf( '[%s]', $shortcode->getTag() ) );
    }
}
