<?php


namespace WW\Services\Testimonials\Models;


class Testimonial
{
    /**
     * @var $instance Testimonial
     */
    private static $instance;
    private $post_type = 'ww_testimonials';

    /**
     * ServiceHandler constructor.
     */
    private function __construct()
    {
       // silence si gold
    }

    /**
     * @return Testimonial
     * @author Kamil Łazarz
     */
    public static function instance() {
        if( self::$instance === null ) {
            self::$instance = new Testimonial();
        }

        return self::$instance;
    }

    /**
     * @param array $args
     * @return int[]|\WP_Post[]
     * @author Kamil Łazarz
     */
    public function getTestimonials( $args = [] ) {

        $args = wp_parse_args(
            $args,
            [
                'post_type' => $this->post_type,
                'posts_per_page' => -1,
                'fields'    => 'ids',
                'order' => 'ASC',
            ] );

        $testimonials = get_posts( $args );

        return $testimonials;
    }

    /**
     * GET Testimonial details
     * @author Kamil Łazarz
     * @param $id
     * @return array
     */
    public function getTestimonial( $id ) {

        $data = [
            'name'  => get_the_title( $id ),
            'review' => get_post_meta( $id, 'ww_review', true ),
            'person_picture' => get_post_meta( $id, 'ww_person_picture', true ),
            'brand_logo' => get_post_meta( $id, 'ww_brand_logo', true ),
            'signature' => get_post_meta( $id, 'ww_signature', true )
        ];

        return $data;
    }
}
