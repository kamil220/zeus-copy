<?php


namespace WW\Services\Testimonials\Controllers;
use Elementor\Widget_Base;

class UX_Widget extends Widget_Base
{
    /**
     * Get widget name.
     */
    public function get_name()
    {
        return 'ww_testimonials';
    }

    /**
     * Get widget title.
     */
    public function get_title()
    {
        return __('Testimonials', ZEUS_SLUG );
    }

    /**
     * Register oEmbed widget controls.
     *
     * Adds different input fields to allow the user to change and customize the widget settings.
     *
     * @since 1.0.0
     * @access protected
     */
    protected function _register_controls() {
        $this->start_controls_section(
            'background_section',
            [
                'label' => __( 'Background', ZEUS_SLUG ),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'background',
            [
                'label' => __( 'URL to embed', 'plugin-name' ),
                'type' => \Elementor\Controls_Manager::MEDIA,
                'input_type' => 'media',
            ]
        );

        $this->end_controls_section();

    }

    /**
     * Get widget icon.
     */
    public function get_icon()
    {
        return 'fas fa-users ww-marker';
    }

    /**
     * Get widget categories.
     */
    public function get_categories()
    {
        return [ 'general', 'webwolf' ];
    }

    /**
     * Render shortcode
     */
    protected function render()
    {
        $shortcode = Shortcode::instance();
        $settings = $this->get_settings_for_display();
        $attachment_id = $settings['background']['id'];

        echo do_shortcode( sprintf( '[%s attachment_id="%d"]', $shortcode->getTag(), $attachment_id ) );
    }
}
