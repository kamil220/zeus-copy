<?php


namespace WW\Services\Testimonials\Controllers;


use WW\Helpers\HTML;

class Shortcode
{

    public $tag = 'ww_testimonials';

    /**
     * @var $instance Shortcode
     */
    private static $instance;

    /**
     * Shortcode constructor.
     */
    private function __construct()
    {
        add_action( 'init', [ $this, 'initialize' ] );
    }

    /**
     * @return Shortcode
     * @author Kamil Łazarz
     */
    public static function instance() {
        if( self::$instance === null ) {
            self::$instance = new Shortcode();
        }

        return self::$instance;
    }

    /**
     * Add shortcode
     * @author Kamil Łazarz
     */
    public function initialize()
    {
        add_shortcode( $this->tag, [ $this, 'callback' ] );
    }

    /**
     * Return shortcode content
     * @author Kamil Łazarz
     * @param $args array
     * @return false|string|HTML
     */
    public function callback( $args = [] ) {

        $args = shortcode_atts([
            'attachment_id' => 0
        ], $args, $this->tag );

        set_query_var( $this->tag . '_attachment_id', $args['attachment_id'] );

        return HTML::getFromFile( COMPONENTS_PATH . 'carousels/testimonials.php' );
    }

    /**
     * Get shortcode tag
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }
}
