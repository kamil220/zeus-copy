<?php


namespace WW\Services\Accordions\Controllers;


use WW\Helpers\HTML;

class Shortcode
{

    public $tag = 'ww_accordion';

    /**
     * @var $instance Shortcode
     */
    private static $instance;

    /**
     * Shortcode constructor.
     */
    private function __construct()
    {
        add_action( 'init', [ $this, 'initialize' ] );
    }

    /**
     * @return Shortcode
     * @author Kamil Łazarz
     */
    public static function instance() {
        if( self::$instance === null ) {
            self::$instance = new Shortcode();
        }

        return self::$instance;
    }

    /**
     * Add shortcode
     * @author Kamil Łazarz
     */
    public function initialize()
    {
        add_shortcode( $this->tag, [ $this, 'callback' ] );
    }

    /**
     * Return shortcode content
     * @param array $args
     * @param $content
     * @return false|string|HTML
     * @author Kamil Łazarz
     */
    public function callback( $args = [], $content ) {

        $args = shortcode_atts([
            'title' => __( 'Title', ZEUS_SLUG ),
            'button-title' => __( 'Read more', ZEUS_SLUG ),
            'button-url' => '',
            'button-type' => '',
            'content' => '',
        ], $args, $this->tag );

        set_query_var( $this->tag . '_title', $args['title'] );
        set_query_var( $this->tag . '_button-title', $args['button-title'] );
        set_query_var( $this->tag . '_button-url', $args['button-url'] );
        set_query_var( $this->tag . '_button-type', $args['button-type'] );
        set_query_var( $this->tag . '_content', $content );

        return HTML::getFromFile( COMPONENTS_PATH . 'accordions/accordions.php' );
    }

    /**
     * Get shortcode tag
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }
}
