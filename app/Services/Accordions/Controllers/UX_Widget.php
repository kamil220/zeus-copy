<?php


namespace WW\Services\Accordions\Controllers;
use Elementor\Widget_Base;

class UX_Widget extends Widget_Base
{
    /**
     * Get widget name.
     */
    public function get_name()
    {
        return 'ww_accordion';
    }

    /**
     * Get widget title.
     */
    public function get_title()
    {
        return __('Accordion', ZEUS_SLUG );
    }

    /**
     * Get widget icon.
     */
    public function get_icon()
    {
        return 'fas fa-users ww-marker';
    }

    /**
     * Get widget categories.
     */
    public function get_categories()
    {
        return [ 'general', 'webwolf' ];
    }

    /**
     * Add ux settings
     */
    protected function _register_controls() {
        $this->start_controls_section(
            'accordions_section',
            [
                'label' => __( 'Accordion', ZEUS_SLUG ),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'title',
            [
                'label' => __( 'Accordion Title', ZEUS_SLUG ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'input_type' => 'text',
            ]
        );

        $this->add_control(
            'button-title',
            [
                'label' => __( 'Read more title', ZEUS_SLUG ),
                'type' => \Elementor\Controls_Manager::TEXT,
                'input_type' => 'text',
            ]
        );

        $this->add_control(
            'button-type',
            [
                'label' => __( 'Button type', ZEUS_SLUG ),
                'type' => \Elementor\Controls_Manager::SELECT,
                'options' => [
                    'accordion' => __( 'Accordion', ZEUS_SLUG ),
                    'url' => __( 'URL', ZEUS_SLUG ),
                ],
                'default' => 'accordion',
            ]
        );

        $this->add_control(
            'button-url',
            [
                'label' => __( 'Button URL', ZEUS_SLUG ),
                'type' => \Elementor\Controls_Manager::URL,
                'input_type' => 'url',
            ]
        );

        $this->add_control(
            'content',
            [
                'label' => __( 'Content', ZEUS_SLUG ),
                'type' => \Elementor\Controls_Manager::WYSIWYG,
                'input_type' => 'wysiwyg',
            ]
        );

        $this->end_controls_section();

    }

    /**
     * Render shortcode
     */
    protected function render()
    {
        $shortcode = Shortcode::instance();
        $settings = $this->get_settings_for_display();

        echo do_shortcode(
            sprintf(
                '[%s title="%s" button-title="%s" button-url="%s" button-type="%s"]%s[/%s]',
                $shortcode->getTag(),
                $settings['title'],
                $settings['button-title'],
                $settings['button-url']['url'],
                $settings['button-type'],
                $settings['content'],
                $shortcode->getTag()
            )
        );
    }
}
