<?php


namespace WW\Services\Orders\Controllers;


class Orders
{
    /**
     * @var $instance Orders
     */
    private static $instance;

    /**
     * ServiceHandler constructor.
     */
    private function __construct() {

    }

    /**
     * @return Orders
     * @author Kamil Łazarz
     */
    public static function instance() {
        if ( self::$instance === null ) {
            self::$instance = new Orders;
        }

        return self::$instance;
    }

    public function addOrder( $data ) {

        $order_id = wp_insert_post( [
            'post_title' => sprintf( __( '%s %s', ZEUS_SLUG ), $data[ 'ww-first-name' ], $data[ 'ww-last-name' ] ),
            'post_type' => 'ww_orders',
            'post_status' => 'publish',
            'post_content' => ''
        ] );

        if ( $order_id === false ) {
            return false;
        }

        foreach ( $data as $key => $value ) {
//            update_post_meta( $order_id, $key, $value );
            update_field( $key, $value, $order_id );
        }

        return true;
    }
}
