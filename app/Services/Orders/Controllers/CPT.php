<?php


namespace WW\Services\Orders\Controllers;


class CPT
{
    /**
     * @var $instance CPT
     */
    private static $instance;

    /**
     * ServiceHandler constructor.
     */
    private function __construct()
    {
        add_action( 'init', [ $this, 'initialize' ] );

        if( !function_exists('acf_add_local_field_group') ) {
            return;
        }

        $this->settings();
    }

    /**
     * @return CPT
     * @author Kamil Łazarz
     */
    public static function instance() {
        if( self::$instance === null ) {
            self::$instance = new CPT;
        }

        return self::$instance;
    }

    /**
     * Initialize ACF methods
     * @author Kamil Łazarz
     */
    public function initialize()
    {
        $labels = array(
            'name'                  => _x( 'Orders', 'Post Type General Name', 'ZEUS_THEME' ),
            'singular_name'         => _x( 'Order', 'Post Type Singular Name', 'ZEUS_THEME' ),
            'menu_name'             => __( 'Orders', 'ZEUS_THEME' ),
            'name_admin_bar'        => __( 'Order', 'ZEUS_THEME' ),
            'archives'              => __( 'Item Archives', 'ZEUS_THEME' ),
            'attributes'            => __( 'Item Attributes', 'ZEUS_THEME' ),
            'parent_item_colon'     => __( 'Parent Item:', 'ZEUS_THEME' ),
            'all_items'             => __( 'All Items', 'ZEUS_THEME' ),
            'add_new_item'          => __( 'Add New Item', 'ZEUS_THEME' ),
            'add_new'               => __( 'Add New', 'ZEUS_THEME' ),
            'new_item'              => __( 'New Item', 'ZEUS_THEME' ),
            'edit_item'             => __( 'Edit Item', 'ZEUS_THEME' ),
            'update_item'           => __( 'Update Item', 'ZEUS_THEME' ),
            'view_item'             => __( 'View Item', 'ZEUS_THEME' ),
            'view_items'            => __( 'View Items', 'ZEUS_THEME' ),
            'search_items'          => __( 'Search Item', 'ZEUS_THEME' ),
            'not_found'             => __( 'Not found', 'ZEUS_THEME' ),
            'not_found_in_trash'    => __( 'Not found in Trash', 'ZEUS_THEME' ),
            'featured_image'        => __( 'Featured Image', 'ZEUS_THEME' ),
            'set_featured_image'    => __( 'Set featured image', 'ZEUS_THEME' ),
            'remove_featured_image' => __( 'Remove featured image', 'ZEUS_THEME' ),
            'use_featured_image'    => __( 'Use as featured image', 'ZEUS_THEME' ),
            'insert_into_item'      => __( 'Insert into item', 'ZEUS_THEME' ),
            'uploaded_to_this_item' => __( 'Uploaded to this item', 'ZEUS_THEME' ),
            'items_list'            => __( 'Items list', 'ZEUS_THEME' ),
            'items_list_navigation' => __( 'Items list navigation', 'ZEUS_THEME' ),
            'filter_items_list'     => __( 'Filter items list', 'ZEUS_THEME' ),
        );

        $args = array(
            'label'                 => __( 'Order', 'ZEUS_THEME' ),
            'labels'                => $labels,
            'supports'              => [ 'title' ],
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 10,
            'menu_icon'             => 'dashicons-cart',
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'capability_type'       => 'page',
        );
        register_post_type( 'ww_orders', $args );
    }

    public function settings() {
        acf_add_local_field_group(array(
            'key' => 'group_5e6d00d84ae38',
            'title' => 'Orders',
            'fields' => array(
                array(
                    'key' => 'field_5e6d00def2b65',
                    'label' => 'First name',
                    'name' => 'ww-first-name',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_5e6d00fbf2b66',
                    'label' => 'Last name',
                    'name' => 'ww-last-name',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_5e6d0108f2b67',
                    'label' => 'E-Mail',
                    'name' => 'ww-your-email',
                    'type' => 'email',
                    'instructions' => '',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                ),
                array(
                    'key' => 'field_5e6d0119f2b68',
                    'label' => 'Company name',
                    'name' => 'ww-company-name',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_5e6d0128f2b69',
                    'label' => 'Company street',
                    'name' => 'ww-company-street',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_5e6d013bf2b6a',
                    'label' => 'Company city',
                    'name' => 'ww-company-city',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_5e6d0146f2b6b',
                    'label' => 'Company ZIP code',
                    'name' => 'ww-company-zip',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_5e6d015ff2b6c',
                    'label' => 'Company Region',
                    'name' => 'ww-company-region',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_5e6d0171f2b6d',
                    'label' => 'Company Country',
                    'name' => 'ww-company-country',
                    'type' => 'text',
                    'instructions' => '',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'maxlength' => '',
                ),
                array(
                    'key' => 'field_5e6d0189f2b6e',
                    'label' => 'Patients',
                    'name' => 'patients',
                    'type' => 'repeater',
                    'instructions' => '',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'collapsed' => '',
                    'min' => 0,
                    'max' => 0,
                    'layout' => 'table',
                    'button_label' => '',
                    'sub_fields' => array(
                        array(
                            'key' => 'field_5e6d019ff2b6f',
                            'label' => 'First name',
                            'name' => 'first-name',
                            'type' => 'text',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'maxlength' => '',
                        ),
                        array(
                            'key' => 'field_5e6d01aaf2b70',
                            'label' => 'Last name',
                            'name' => 'last-name',
                            'type' => 'text',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'maxlength' => '',
                        ),
                        array(
                            'key' => 'field_5e6d01aff2b71',
                            'label' => 'Age',
                            'name' => 'age',
                            'type' => 'number',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'min' => '',
                            'max' => '',
                            'step' => '',
                        ),
                        array(
                            'key' => 'field_5e6d023df2b74',
                            'label' => 'Gender',
                            'name' => 'gender',
                            'type' => 'select',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'choices' => array(
                                'Female' => 'Female',
                                'Male' => 'Male',
                            ),
                            'default_value' => array(
                            ),
                            'allow_null' => 0,
                            'multiple' => 0,
                            'ui' => 0,
                            'return_format' => 'value',
                            'ajax' => 0,
                            'placeholder' => '',
                        ),
                        array(
                            'key' => 'field_5e6d01bbf2b72',
                            'label' => 'Amputation level',
                            'name' => 'amp-lvl',
                            'type' => 'select',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'choices' => array(
                                'below' => 'Below Elbow',
                                'above' => 'Above Elbow',
                            ),
                            'default_value' => array(
                            ),
                            'allow_null' => 0,
                            'multiple' => 0,
                            'ui' => 0,
                            'return_format' => 'value',
                            'ajax' => 0,
                            'placeholder' => '',
                        ),
                        array(
                            'key' => 'field_5e6d021df2b73',
                            'label' => 'Prothesis Used Before',
                            'name' => 'used-before',
                            'type' => 'true_false',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'message' => '',
                            'default_value' => 0,
                            'ui' => 0,
                            'ui_on_text' => '',
                            'ui_off_text' => '',
                        ),
                        array(
                            'key' => 'field_5e6d0264f2b75',
                            'label' => 'Prothesis site',
                            'name' => 'site',
                            'type' => 'select',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'choices' => array(
                                'Left' => 'Left',
                                'Right' => 'Right',
                            ),
                            'default_value' => array(
                            ),
                            'allow_null' => 0,
                            'multiple' => 0,
                            'ui' => 0,
                            'return_format' => 'value',
                            'ajax' => 0,
                            'placeholder' => '',
                        ),
                        array(
                            'key' => 'field_5e6d02d4f2b76',
                            'label' => 'Prothesis color',
                            'name' => 'color',
                            'type' => 'select',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'choices' => array(
                                'black' => 'Black, No Glove',
                                'Glove #1' => 'Glove #1',
                                'Glove #2' => 'Glove #2',
                                'Glove #3' => 'Glove #3',
                                'Glove #4' => 'Glove #4',
                                'Glove #5' => 'Glove #5',
                                'Glove #6' => 'Glove #6',
                                'Glove #7' => 'Glove #7',
                                'Glove #8' => 'Glove #8',
                                'Glove #9' => 'Glove #9',
                                'Glove #10' => 'Glove #10',
                            ),
                            'default_value' => array(
                            ),
                            'allow_null' => 0,
                            'multiple' => 0,
                            'ui' => 0,
                            'return_format' => 'value',
                            'ajax' => 0,
                            'placeholder' => '',
                        ),
                    ),
                ),
            ),
            'location' => array(
                array(
                    array(
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'ww_orders',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => true,
            'description' => '',
        ));

    }
}
