<?php


namespace WW\Services\Orders;

/**
 * Class ServiceHandler
 * @package WW\Services\Orders
 * @author Kamil Łazarz
 */
class ServiceHandler
{
    /**
     * @var $instance ServiceHandler
     */
    private static $instance;

    /**
     * ServiceHandler constructor.
     */
    private function __construct()
    {
        $this->register();
    }

    /**
     * @return ServiceHandler
     * @author Kamil Łazarz
     */
    public static function instance() {
        if( self::$instance === null ) {
            self::$instance = new ServiceHandler;
        }

        return self::$instance;
    }

    /**
     * Initialize
     * @author Kamil Łazarz
     */
    private function register()
    {
        Controllers\CPT::instance();
    }
}
