<?php


namespace WW\Services\CF7;

use WW\Helpers\HTML;

/**
 * Class ServiceHandler
 * @package WW\Services\CF7
 * @author Kamil Łazarz
 */
class ThankYouTemplates
{
    /**
     * @var $instance ServiceHandler
     */
    private static $instance;

    /**
     * ServiceHandler constructor.
     */
    private function __construct()
    {
        $this->initialize();
    }

    /**
     * @return ServiceHandler
     * @author Kamil Łazarz
     */
    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new ThankYouTemplates;
        }

        return self::$instance;
    }

    /**
     * Initialize events
     * @author Kamil Łazarz
     */
    public function initialize()
    {
        /* Add settings to contact form 7 */
        add_filter('wpcf7_editor_panels', [$this, 'panel'], 20, 1);
        add_action('wpcf7_after_save', [$this, 'save']);

        /* Add ajax request */
        add_action('wp_ajax_ww_cf7_submit_form', [ $this, 'handleForm' ] );
        add_action('wp_ajax_nopriv_ww_cf7_submit_form', [ $this, 'handleForm' ] );
    }

    public function panel($panels)
    {
        $panels['cf7-thank-you-page'] = [
            'title' => __('Thank you page', ZEUS_SLUG),
            'callback' => [$this, 'fields']
        ];

        return $panels;
    }

    /**
     * Add fields to thank you settings page
     * @author Kamil Łazarz
     * @param $post
     */
    public function fields($post)
    {
        ?>
        <p class="description">
            <label for="ww_custom_thank_you_page"><?= __('Enable dynamic thank you page', ZEUS_SLUG); ?><br/>
                <?php $enabled = get_post_meta($post->id, 'ww_custom_thank_you_page', true); ?>

                <input type="checkbox" id="ww_custom_thank_you_page" name="ww_custom_thank_you_page" value="true"
                       <?= $enabled == 'true' ? ' checked="checked" ' : ''; ?>data-config-field="registration.ww_custom_thank_you_page"/>
            </label>
        </p>

        <p class="description">
            <label for="ww_custom_thank_you_page_layout"><?= __('Choose thank you page layout', ZEUS_SLUG); ?><br/>
                <?php
                    $layout = get_post_meta($post->id, 'ww_custom_thank_you_page_layout', true);
                ?>
                <select name="ww_custom_thank_you_page_layout">
                    <option value="thank-you.php" <?=selected( $layout, 'thank-you.php' );?>>Default</option>
                    <option value="thank-you-order.php" <?=selected( $layout, 'thank-you-order.php' );?>>Order Form</option>
                    <option value="thank-you-interest.php" <?=selected( $layout, 'thank-you-interest.php' );?>>Interest Form</option>
                </select>
            </label>
        </p>
        <?php
    }


    /**
     * Save settings from thank you page
     * @author Kamil Łazarz
     * @param $post
     * @return mixed
     */
    public function save($post)
    {
        $enabled = isset($_POST['ww_custom_thank_you_page']) && $_POST['ww_custom_thank_you_page'] == 'true' ? $_POST['ww_custom_thank_you_page'] : false;
        $layout = isset($_POST['ww_custom_thank_you_page_layout']) ? $_POST['ww_custom_thank_you_page_layout'] : 'thank-you.php';

        \update_post_meta($post->id, 'ww_custom_thank_you_page', $enabled);
        \update_post_meta($post->id, 'ww_custom_thank_you_page_layout', $layout);

        return $post;
    }

    public function handleForm() {

        if( !isset( $_POST[ 'form_id' ] ) || empty( $_POST[ 'form_id' ] ) ) {
            wp_send_json_error( [ 'message' => __( 'Form id canno\'t be empty', ZEUS_SLUG ) ] );
        }

        $form_id = $_POST[ 'form_id' ];
        $enabled_thank_you_page = get_post_meta($form_id, 'ww_custom_thank_you_page', true);

        if( !$enabled_thank_you_page ) {
            wp_send_json_success( [
                    'status' => 'error',
                    'message'   => __( 'This form doesnt use custom thank you message.', ZEUS_SLUG )
            ]);
        }

        $thank_you_page_layout = get_post_meta($form_id, 'ww_custom_thank_you_page_layout', true);
        $thank_you_page_layout = $thank_you_page_layout ? $thank_you_page_layout : 'thank-you.php';

        wp_send_json_success( [
            'status' => 'success',
            'html' => HTML::getFromFile( TEMPLATES_PATH . 'CF7/' . $thank_you_page_layout )
        ]);
    }
}
