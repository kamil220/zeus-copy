<?php


namespace WW\Services\CF7;


use WW\Services\Orders\Controllers\Orders;

class OrderForm
{
    /**
     * @var $instance OrderForm
     */
    private static $instance;

    /**
     * OrderForm constructor.
     */
    private function __construct()
    {
        $this->initialize();
    }

    /**
     * @return OrderForm
     * @author Kamil Łazarz
     */
    public static function instance()
    {
        if (self::$instance === null) {
            self::$instance = new OrderForm;
        }

        return self::$instance;
    }

    /**
     * Initialize events
     * @author Kamil Łazarz
     */
    public function initialize() {
        add_action( 'wpcf7_before_send_mail', [ $this, 'createOrder' ], 9 );
    }

    /**
     * Create order
     * @author Kamil Łazarz
     * @param $data
     * @return mixed
     */
    public function createOrder( $data )  {

        if (!isset($_POST['_wpcf7']) || empty($_POST['_wpcf7'])) {
            return $data;
        }



        $form_id = $_POST['_wpcf7'];

        $patients_amount = $_POST['ww-units-numner'];

        $data_keys = [
            'ww-first-name',
            'ww-last-name',
            'ww-your-email',
            'ww-company-name',
            'ww-company-street',
            'ww-company-city',
            'ww-company-zip',
            'ww-company-region',
            'ww-company-country',
            'ww-units-numner'
        ];

        $patients_data_keys = [
            'first-name',
            'last-name',
            'age',
            'amp-lvl',
            'used-before',
        ];

        $patients_hand_data_keys = [
            'gender',
            'site',
            'color',
        ];

        $order_data = [];

        foreach ( $data_keys as $data_key ) {
            $order_data[ $data_key ] = isset( $_POST[ $data_key ] ) ? $_POST[ $data_key ] : '';
        }

        $i = 1;

        while( $i <= $patients_amount ) {

            foreach ( $patients_data_keys as $patient_key ) {
                $order_data[ 'patients' ][ $i ][ $patient_key ] = isset( $_POST[ 'ww-patient-' . $patient_key . '-' . $i ] ) ? $_POST[ 'ww-patient-' . $patient_key . '-' . $i ] : '';
            }

            foreach( $patients_hand_data_keys as $hand_key ) {
                $order_data[ 'patients' ][ $i ][ $hand_key ] = isset( $_POST[ 'ww-hand-' . $hand_key . '-' . $i ] ) ? $_POST[ 'ww-hand-' . $hand_key . '-' . $i ] : '';
            }

            $i++;
        }

        $ordersHandler = Orders::instance();
        $ordersHandler->addOrder( $order_data );

        return $data;
    }
}
