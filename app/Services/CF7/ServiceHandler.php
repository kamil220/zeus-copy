<?php


namespace WW\Services\CF7;

/**
 * Class ServiceHandler
 * @package WW\Services\CF7
 * @author Kamil Łazarz
 */
class ServiceHandler
{
    /**
     * @var $instance ServiceHandler
     */
    private static $instance;

    /**
     * ServiceHandler constructor.
     */
    private function __construct()
    {
        $this->initialize();
    }

    /**
     * @return ServiceHandler
     * @author Kamil Łazarz
     */
    public static function instance() {
        if( self::$instance === null ) {
            self::$instance = new ServiceHandler;
        }

        return self::$instance;
    }

    /**
     * Initialize ACF methods
     * @author Kamil Łazarz
     */
    private function initialize()
    {
        /* Include plugin only if there are not added before  */
        if( !defined( 'WPCF7_VERSION' ) ) {
            add_action('admin_notices', [ $this, 'displayError' ] );
            return;
        }

        ThankYouTemplates::instance();
        OrderForm::instance();
    }

    /**
     * Display error if contact form 7 plugin is not activated
     * @author Kamil Łazarz
     */
    function displayError() {

        $screen_id = get_current_screen()->id;

        if( $screen_id  !== 'plugins' && $screen_id !== 'themes' ) {
            return;
        }

        ?>
        <div class="notice notice-error is-dismissible">
            <p><?php _e('Zeus theme requires a Contact Form 7 plugin.', ZEUS_SLUG ); ?></p>
        </div>
        <?php
    }
}
