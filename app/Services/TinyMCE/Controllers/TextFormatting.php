<?php


namespace WW\Services\TinyMCE\Controllers;

class TextFormatting
{
    /**
     * @var $instance TextFormatting
     */
    private static $instance;

    /**
     * Shortcode constructor.
     */
    private function __construct()
    {
        add_action( 'init', [ $this, 'initialize' ] );
    }

    /**
     * @return TextFormatting
     * @author Kamil Łazarz
     */
    public static function instance() {
        if( self::$instance === null ) {
            self::$instance = new TextFormatting();
        }

        return self::$instance;
    }

    /**
     * Add shortcode
     * @author Kamil Łazarz
     */
    public function initialize()
    {
        if ( ! current_user_can( 'edit_posts' ) && ! current_user_can( 'edit_pages' ) ) {
            return;
        }

        if ( get_user_option( 'rich_editing' ) !== 'true' ) {
            return;
        }

        add_filter( 'mce_buttons_2', [ $this, 'textJustify' ], 5 );
    }

    /**
     * Add text justify button to TinyMCE
     * @author Kamil Łazarz
     * @param $mce_buttons
     * @return array
     */
    public function textJustify( $mce_buttons ) {
        $mce_buttons[] = 'alignjustify';

        return $mce_buttons;
    }
}
