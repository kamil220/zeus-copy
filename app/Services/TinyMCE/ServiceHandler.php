<?php


namespace WW\Services\TinyMCE;

/**
 * Class ServiceHandler
 * @package WW\Services\TinyMCE
 * @author Kamil Łazarz
 */
class ServiceHandler
{
    /**
     * @var $instance ServiceHandler
     */
    private static $instance;

    /**
     * ServiceHandler constructor.
     */
    private function __construct()
    {
        $this->initialize();
    }

    /**
     * @return ServiceHandler
     * @author Kamil Łazarz
     */
    public static function instance() {
        if( self::$instance === null ) {
            self::$instance = new ServiceHandler;
        }

        return self::$instance;
    }

    /**
     * Initialize ACF methods
     * @author Kamil Łazarz
     */
    public function initialize()
    {
        Controllers\TextFormatting::instance();
    }
}
