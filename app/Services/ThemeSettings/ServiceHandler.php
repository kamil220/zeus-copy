<?php


namespace WW\Services\ThemeSettings;

/**
 * Class ServiceHandler
 * @package WW\Services\ThemeSettings
 * @author Kamil Łazarz
 */
class ServiceHandler
{
    /**
     * @var $instance ServiceHandler
     */
    private static $instance;

    /**
     * ServiceHandler constructor.
     */
    private function __construct()
    {
        $this->initialize();
    }

    /**
     * @return ServiceHandler
     * @author Kamil Łazarz
     */
    public static function instance() {
        if( self::$instance === null ) {
            self::$instance = new ServiceHandler;
        }

        return self::$instance;
    }

    /**
     * Initialize Theme Settings hooks
     * @author Kamil Łazarz
     */
    private function initialize()
    {
        add_action( 'elementor/elements/categories_registered', [ $this, 'widgetsCategory' ] );
        add_action( 'elementor/editor/after_enqueue_styles', [ $this, 'uxStyles' ] );
        add_action('elementor/element/button/section_button/before_section_end', [ $this, 'customButtons' ] );
        add_filter('wpcf7_autop_or_not', '__return_false');
    }

    public function uxStyles() {
        wp_enqueue_style(
            'zeus-admin-css',
            PUBLIC_URL . 'css/zeus-admin.css',
            [],
            '1.0.1',
            'all'
        );
    }

    /**
     * Add elementor widgets category
     * @author Kamil Łazarz
     * @param $elements_manager
     */
    public function widgetsCategory( $elements_manager ){
        $elements_manager->add_category(
            'webwolf',
            [
                'title' => __( 'Web Wolf', ZEUS_SLUG ),
                'icon' => 'dashicons-zeus-dashboard',
            ]
        );
    }

    /**
     * Add additional button types
     * @author Kamil Łazarz
     * @param $instance
     */
    public function customButtons( $instance ) {
        $controls = $instance->get_controls();
        $controls['button_type']['options'] += [
            'custom_1' => __('Custom 1'),
            'custom_2' => __('Custom 2'),
        ];
        $instance->update_control('button_type', $controls['button_type']);
    }
}
