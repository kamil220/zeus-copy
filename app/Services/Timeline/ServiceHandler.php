<?php


namespace WW\Services\Timeline;

use Elementor\Plugin as Elementor;

/**
 * Class ServiceHandler
 * @package WW\Services\Timeline
 * @author Kamil Łazarz
 */
class ServiceHandler
{
    /**
     * @var $instance ServiceHandler
     */
    private static $instance;

    /**
     * ServiceHandler constructor.
     */
    private function __construct()
    {
        $this->initialize();
        add_action( 'elementor/widgets/widgets_registered', [ $this, 'registerWidget' ] );
    }

    /**
     * @return ServiceHandler
     * @author Kamil Łazarz
     */
    public static function instance() {
        if( self::$instance === null ) {
            self::$instance = new ServiceHandler;
        }

        return self::$instance;
    }

    /**
     * Initialize ACF methods
     * @author Kamil Łazarz
     */
    public function initialize()
    {
        Controllers\CPT::instance();
        Controllers\Shortcode::instance();
    }

    /**
     *
     * Register widget
     * @author Kamil Łazarz
     * @throws \Exception
     */
    public function registerWidget() {
        Elementor::instance()->widgets_manager->register_widget_type( new Controllers\UX_Widget() );
    }
}
