<?php


namespace WW\Services\Timeline\Models;


class Timeline
{
    /**
     * @var $instance Timeline
     */
    private static $instance;
    private $post_type = 'ww_timeline';

    /**
     * ServiceHandler constructor.
     */
    private function __construct()
    {
       // silence si gold
    }

    /**
     * @return Timeline
     * @author Kamil Łazarz
     */
    public static function instance() {
        if( self::$instance === null ) {
            self::$instance = new Timeline();
        }

        return self::$instance;
    }

    /**
     * @param array $args
     * @return int[]|\WP_Post[]
     * @author Kamil Łazarz
     */
    public function getTimelineItems( $args = [] ) {

        $args = wp_parse_args(
            $args,
            [
                'post_type' => $this->post_type,
                'posts_per_page' => 7,
                'fields'    => 'ids',
                'meta_key'  => 'ww_timeline_choose_year',
                'orderby'   => 'meta_value_num',
                'order' => 'ASC',
            ] );

        $timeline_items = get_posts( $args );

        return $timeline_items;
    }

    /**
     * GET Testimonial details
     * @author Kamil Łazarz
     * @param $id
     * @return array
     */
    public function getTimelineItem( $id ) {

        $data = [
            'icon'  => wp_get_attachment_image( get_post_meta( $id, 'ww_timeline_choose_icon', true ) ),
            'color' => get_post_meta( $id, 'ww_timeline_choose_color', true ),
            'year' => get_post_meta( $id, 'ww_timeline_choose_year', true ),
            'content' => get_post_meta( $id, 'ww_timeline_enter_your_content', true ),
            'position' => get_post_meta( $id, 'ww_timeline_choose_item_position', true )
        ];

        return $data;
    }
}
