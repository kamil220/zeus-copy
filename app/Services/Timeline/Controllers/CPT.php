<?php


namespace WW\Services\Timeline\Controllers;


class CPT
{
    /**
     * @var $instance CPT
     */
    private static $instance;

    /**
     * ServiceHandler constructor.
     */
    private function __construct()
    {
        add_action( 'init', [ $this, 'initialize' ] );

        if( !function_exists('acf_add_local_field_group') ) {
            return;
        }

        $this->settings();
    }

    /**
     * @return CPT
     * @author Kamil Łazarz
     */
    public static function instance() {
        if( self::$instance === null ) {
            self::$instance = new CPT;
        }

        return self::$instance;
    }

    /**
     * Initialize ACF methods
     * @author Kamil Łazarz
     */
    public function initialize()
    {
        $labels = array(
            'name'                  => _x( 'Timeline', 'Post Type General Name', 'ZEUS_THEME' ),
            'singular_name'         => _x( 'Timeline', 'Post Type Singular Name', 'ZEUS_THEME' ),
            'menu_name'             => __( 'Timeline', 'ZEUS_THEME' ),
            'name_admin_bar'        => __( 'Timeline', 'ZEUS_THEME' ),
            'archives'              => __( 'Item Archives', 'ZEUS_THEME' ),
            'attributes'            => __( 'Item Attributes', 'ZEUS_THEME' ),
            'parent_item_colon'     => __( 'Parent Item:', 'ZEUS_THEME' ),
            'all_items'             => __( 'All Items', 'ZEUS_THEME' ),
            'add_new_item'          => __( 'Add New Item', 'ZEUS_THEME' ),
            'add_new'               => __( 'Add New', 'ZEUS_THEME' ),
            'new_item'              => __( 'New Item', 'ZEUS_THEME' ),
            'edit_item'             => __( 'Edit Item', 'ZEUS_THEME' ),
            'update_item'           => __( 'Update Item', 'ZEUS_THEME' ),
            'view_item'             => __( 'View Item', 'ZEUS_THEME' ),
            'view_items'            => __( 'View Items', 'ZEUS_THEME' ),
            'search_items'          => __( 'Search Item', 'ZEUS_THEME' ),
            'not_found'             => __( 'Not found', 'ZEUS_THEME' ),
            'not_found_in_trash'    => __( 'Not found in Trash', 'ZEUS_THEME' ),
            'featured_image'        => __( 'Featured Image', 'ZEUS_THEME' ),
            'set_featured_image'    => __( 'Set featured image', 'ZEUS_THEME' ),
            'remove_featured_image' => __( 'Remove featured image', 'ZEUS_THEME' ),
            'use_featured_image'    => __( 'Use as featured image', 'ZEUS_THEME' ),
            'insert_into_item'      => __( 'Insert into item', 'ZEUS_THEME' ),
            'uploaded_to_this_item' => __( 'Uploaded to this item', 'ZEUS_THEME' ),
            'items_list'            => __( 'Items list', 'ZEUS_THEME' ),
            'items_list_navigation' => __( 'Items list navigation', 'ZEUS_THEME' ),
            'filter_items_list'     => __( 'Filter items list', 'ZEUS_THEME' ),
        );

        $args = array(
            'label'                 => __( 'Timeline', 'ZEUS_THEME' ),
            'labels'                => $labels,
            'supports'              => [ 'title' ],
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 10,
            'menu_icon'             => 'dashicons-clock',
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'capability_type'       => 'page',
        );
        register_post_type( 'ww_timeline', $args );
    }

    public function settings() {
        acf_add_local_field_group(array(
            'key' => 'group_5e601ef2b6cb7',
            'title' => 'Timeline',
            'fields' => array(
                array(
                    'key' => 'field_5e601f2a8b477',
                    'label' => 'Choose icon',
                    'name' => 'ww_timeline_choose_icon',
                    'type' => 'image',
                    'instructions' => '',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'return_format' => 'id',
                    'preview_size' => 'medium',
                    'library' => 'all',
                    'min_width' => '',
                    'min_height' => '',
                    'min_size' => '',
                    'max_width' => 100,
                    'max_height' => 100,
                    'max_size' => '',
                    'mime_types' => '',
                ),
                array(
                    'key' => 'field_5e6020508b478',
                    'label' => 'Choose color',
                    'name' => 'ww_timeline_choose_color',
                    'type' => 'color_picker',
                    'instructions' => '',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                ),
                array(
                    'key' => 'field_5e6020a08b479',
                    'label' => 'Choose year',
                    'name' => 'ww_timeline_choose_year',
                    'type' => 'number',
                    'instructions' => '',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => 2020,
                    'placeholder' => '',
                    'prepend' => '',
                    'append' => '',
                    'min' => '',
                    'max' => '',
                    'step' => '',
                ),
                array(
                    'key' => 'field_5e60210d8b47a',
                    'label' => 'Enter your content',
                    'name' => 'ww_timeline_enter_your_content',
                    'type' => 'textarea',
                    'instructions' => '',
                    'required' => 1,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'maxlength' => '',
                    'rows' => '',
                    'new_lines' => '',
                ),
            ),
            'location' => array(
                array(
                    array(
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'ww_timeline',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => true,
            'description' => '',
        ));
    }
}
