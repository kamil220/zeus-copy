<?php


namespace WW\Services\Timeline\Controllers;
use Elementor\Widget_Base;

class UX_Widget extends Widget_Base
{
    /**
     * Get widget name.
     */
    public function get_name()
    {
        return 'ww_timeline';
    }

    /**
     * Get widget title.
     */
    public function get_title()
    {
        return __('Timeline', ZEUS_SLUG );
    }

    /**
     * Get widget icon.
     */
    public function get_icon()
    {
        return 'fas fa-users ww-marker';
    }

    /**
     * Get widget categories.
     */
    public function get_categories()
    {
        return [ 'general', 'webwolf' ];
    }

    /**
     * Render shortcode
     */
    protected function render()
    {
        $shortcode = Shortcode::instance();
        echo do_shortcode( sprintf( '[%s]', $shortcode->getTag() ) );
    }
}
