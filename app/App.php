<?php


namespace WW;

use WW\Services\ACF\ServiceHandler as ACF;
use WW\Services\CF7\ServiceHandler as CF7;
use WW\Services\ThemeSettings\ServiceHandler as ThemeSettings;
use WW\Services\OurTeam\ServiceHandler as OurTeam;
use WW\Services\Testimonials\ServiceHandler as Testimonials;
use WW\Services\Timeline\ServiceHandler as Timeline;
use WW\Services\ImageSlider\ServiceHandler as ImageSlider;
use WW\Services\Orders\ServiceHandler as Orders;
use WW\Services\TinyMCE\ServiceHandler as TinyMCE;
use WW\Services\Accordions\ServiceHandler as Accordions;

class App
{
    private static $instance;
    private $version;

    /**
     * App constructor.
     */
    private function __construct()
    {
        /* Setup version */
        if( !isset( $this->version ) || empty( $this->version ) ) {
            $this->version = DEV_MODE ? time() : ZEUS_VERSION;
        }

        /* Load services */
        $this->loadServices();

        /* Load scripts */
        add_action( 'wp_enqueue_scripts', [ $this, 'scripts' ], 15 );
        add_action( 'admin_enqueue_scripts', [ $this, 'adminScripts' ], 15 );
    }

    /**
     * Instance app class
     * @author Kamil Łazarz
     * @return App
     */
    public static function instance() {

        if( self::$instance === null ) {
            self::$instance = new App;
        }

        return self::$instance;
    }

    public function scripts() {

        wp_enqueue_style(
            'zeus-theme-css',
            get_stylesheet_directory_uri() . '/style.css',
            [ 'astra-theme-css' ],
            $this->version,
            'all'
        );

        wp_enqueue_script(
            'zeus-scripts',
            PUBLIC_URL . 'js/app.js',
            [],
            $this->version,
            true
        );

        wp_localize_script(
            'zeus-scripts',
            'zeus_object', [
                'ajax_url' => admin_url( 'admin-ajax.php' )
            ]
        );
    }

    public function adminScripts() {
        wp_enqueue_style(
            'zeus-admin-css',
            PUBLIC_URL . 'css/zeus-admin.css',
            [],
            $this->version,
            'all'
        );
    }

    /**
     * Load application services
     * @author Kamil Łazarz
     */
    private function loadServices() {
        ACF::instance();
        CF7::instance();
        ThemeSettings::instance();
        OurTeam::instance();
        Testimonials::instance();
        Timeline::instance();
        ImageSlider::instance();
        Orders::instance();
        TinyMCE::instance();
        Accordions::instance();
    }
}
