<?php
/**
 * Zeus Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Zeus
 * @since 1.0.0
 */

require_once 'app/AppLoader.php';
